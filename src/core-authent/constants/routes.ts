const routes = {
  login: "/login",
  register: "/register",
  forgot: "/forgot",
  dashboard: "/admin/dashboard",
  home: "/home",
  posts: "/posts",
  profile: "/profile",
  adduser: "/adduser",
  addpost: "/addpser",
  profilelist: "/profile-list",
  setting: "/setting",
};

export default routes;
